PGDMP                     
    u            pgpooldb    9.6.3    9.6.3     _           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            `           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            a           1262    36295    pgpooldb    DATABASE     �   CREATE DATABASE pgpooldb WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE pgpooldb;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            b           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    4                        3079    12387    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            c           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    2                        3079    36296 	   adminpack 	   EXTENSION     A   CREATE EXTENSION IF NOT EXISTS adminpack WITH SCHEMA pg_catalog;
    DROP EXTENSION adminpack;
                  false            d           0    0    EXTENSION adminpack    COMMENT     M   COMMENT ON EXTENSION adminpack IS 'administrative functions for PostgreSQL';
                       false    1            �            1259    36305    person    TABLE     �   CREATE TABLE person (
    id bigint NOT NULL,
    firstname text NOT NULL,
    lastname text NOT NULL,
    age text NOT NULL,
    sex boolean NOT NULL,
    address bigint
);
    DROP TABLE public.person;
       public         postgres    false    4            �            1259    36311    Person_id_seq    SEQUENCE     q   CREATE SEQUENCE "Person_id_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public."Person_id_seq";
       public       postgres    false    4    186            e           0    0    Person_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE "Person_id_seq" OWNED BY person.id;
            public       postgres    false    187            �            1259    36318    address    TABLE     �   CREATE TABLE address (
    id bigint NOT NULL,
    street text NOT NULL,
    number integer NOT NULL,
    postal_code integer NOT NULL,
    country text
);
    DROP TABLE public.address;
       public         postgres    false    4            �            1259    36316    address_id_seq    SEQUENCE     p   CREATE SEQUENCE address_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.address_id_seq;
       public       postgres    false    4    189            f           0    0    address_id_seq    SEQUENCE OWNED BY     3   ALTER SEQUENCE address_id_seq OWNED BY address.id;
            public       postgres    false    188            �            1259    36347    person_address_seq    SEQUENCE     t   CREATE SEQUENCE person_address_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.person_address_seq;
       public       postgres    false    4    186            g           0    0    person_address_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE person_address_seq OWNED BY person.address;
            public       postgres    false    190            �           2604    36321 
   address id    DEFAULT     Z   ALTER TABLE ONLY address ALTER COLUMN id SET DEFAULT nextval('address_id_seq'::regclass);
 9   ALTER TABLE public.address ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    188    189    189            �           2604    36313 	   person id    DEFAULT     Z   ALTER TABLE ONLY person ALTER COLUMN id SET DEFAULT nextval('"Person_id_seq"'::regclass);
 8   ALTER TABLE public.person ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    187    186            �           2604    36349    person address    DEFAULT     b   ALTER TABLE ONLY person ALTER COLUMN address SET DEFAULT nextval('person_address_seq'::regclass);
 =   ALTER TABLE public.person ALTER COLUMN address DROP DEFAULT;
       public       postgres    false    190    186            h           0    0    Person_id_seq    SEQUENCE SET     6   SELECT pg_catalog.setval('"Person_id_seq"', 9, true);
            public       postgres    false    187            [          0    36318    address 
   TABLE DATA               D   COPY address (id, street, number, postal_code, country) FROM stdin;
    public       postgres    false    189          i           0    0    address_id_seq    SEQUENCE SET     5   SELECT pg_catalog.setval('address_id_seq', 2, true);
            public       postgres    false    188            X          0    36305    person 
   TABLE DATA               E   COPY person (id, firstname, lastname, age, sex, address) FROM stdin;
    public       postgres    false    186   e       j           0    0    person_address_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('person_address_seq', 8, true);
            public       postgres    false    190            �           2606    36315    person Person_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY person
    ADD CONSTRAINT "Person_pkey" PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.person DROP CONSTRAINT "Person_pkey";
       public         postgres    false    186    186            �           2606    36326    address address_pkey 
   CONSTRAINT     K   ALTER TABLE ONLY address
    ADD CONSTRAINT address_pkey PRIMARY KEY (id);
 >   ALTER TABLE ONLY public.address DROP CONSTRAINT address_pkey;
       public         postgres    false    189    189            [   O   x�3�,*MUHI-V(�--J+�4�46�05�tr�sv�2�L�/�I-K,J�JNLN�(=��$��������4F��� ǹ�      X   v   x�]�K
�@D�է����QaV.��͢Er�8�MѼ�G��N�A�S�Ac`��B氮����(%��̦㋦�༹�g�5m��;Z.��:Y��ޕfR��`�L�{��0     