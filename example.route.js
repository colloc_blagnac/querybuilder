const QueryBuilder  = require('./tools/tool.querybuilder');

let pool = new QueryBuilder({
    dbName:process.env.dbName,
    dbUser: process.env.dbUser,
    dbHost: process.env.dbHost,
    dbPort: process.env.dbPort,
    dbPassword: process.env.dbPassword
});

class Route{

    static start(app){

        // "Welcome" API
        app.get('/', (req, res)=>{
            res.status(200).send("Welcome");
        });

        // GET synchronously person API
        app.get('/person/sync', async (req, res)=>{

            let data = await
                pool.select("*")
                    .from("person")
                    .where("firstname")
                    .in("toto")
                    .syncExecute();

            res.status(200).json(data.rows);
        });

        // GET person API
        app.get('/person', (req, res)=>{

            pool.select("*")
                .from("person")
                .where({
                    "firstname": 'toto',
                    "age": [25, 26]
                })
                .execute()
                .then( data => res.status(200).json(data.rows) )
                .catch( err => res.status(500).send(`ouch! error: ${err}`));
        });

        //GET person with subQuery
        app.get('/personSubQuery', (req, res)=>{

            pool.select("*")
                .from("person")
                .where("firstname")
                .startSubQuery("IN")
                    .select("firstname")
                    .from("person")
                .endSubQuery()
                .execute()
                .then( data => res.status(200).json(data.rows) )
                .catch( err => res.status(500).send(`ouch! error: ${err}`));
        });

        //GET COUNT(id) person
        app.get('/personCountId', (req, res)=>{

            pool.maxConnectionTimeout(0.1)
                .select(["firstname", "lastname", "COUNT (id) "])
                .from("person")
                .groupBy(["firstname", "lastname"])
                .having(" COUNT(id) <= 10")
                .execute()
                .then( data => res.status(200).json(data.rows) )
                .catch( err => res.status(500).send(`ouch! error: ${err}`));
        });

        // POST person API
        app.post('/person', async (req, res)=>{
            let person = req.body.person;
            let columns = Object.keys(person);
            let values = Object.values(person);

            let data = await
                pool.insert("person", columns)
                    .values(values)
                    .syncExecute();

            res.status(200).json(data.rows[0]);
        });

        // POST person API
        app.get('/address', async (req, res)=>{

            let data = await
                pool.select("*")
                    .from("person")
                    .innerJoin({person: 'address', address: 'id'})
                    .where("firstname")
                    .in("toto")
                    .syncExecute();

            res.status(200).json(data.rows);
        });

        // PUT person API
        app.put('/person', async (req, res)=>{
            let person = req.body.person;
            let id = person.id;
            delete person.id;

            let data =
                pool.update("person")
                    .set(person)
                    .where("id")
                    .equal(id)
                    .syncExecute()
                    .catch( err => res.status(500).json(err) );

            res.status(200).json(data);
        });

        // DELETE person API
        app.delete('/person', async (req, res)=>{
            let person = req.body.person;

            let data =
                pool.delete()
                    .from("person")
                    .where("id")
                    .equal(person.id)
                    .syncExecute()
                    .catch( err => res.status(500).json(err) );

            res.status(200).json(data);
        });

    }
}

module.exports.Route = Route;