module.exports = function(grunt) {

    require('load-grunt-tasks')(grunt);
    grunt.initConfig({
        clean: {
            es5:  ["./es5"],
            doc2: ["doc2"],
            first: ["./doc", "./es5"]
        },

        babel: {
            options: {
                sourceMap: false,
                presets: ['es2015'],
                plugins: ["syntax-async-functions","transform-regenerator"]
            },
            dist: {
                files:[
                    {
                        expand: true,
                        cwd: "./tools",
                        src: ["**/*.js"],
                        dest: "./es5/tools/"
                    }
                ]
            }
        },

        copy: {
            main: {
                src: './doc/**',
                dest: './es5/',
            },
        },

        jsdoc : {
            dist : {
                src: ['./tools/*.js', 'README.md'],
                options: {
                    destination: 'doc',
                    template : "./node_modules/ink-docstrap/template",
                    configure : "./node_modules/ink-docstrap/template/jsdoc.conf.json"
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks("grunt-babel");
    grunt.loadNpmTasks('grunt-jsdoc');
    grunt.loadNpmTasks('grunt-jsdoc-to-markdown');
    grunt.loadNpmTasks('grunt-contrib-copy');

    grunt.registerTask("es5", ["clean:es5", "babel", "jsdoc", "copy"]);
    grunt.registerTask("doc", ["clean:first", "jsdoc"]);
    grunt.registerTask("default", ()=> console.info(`
    =========================================
    *                                       *
    *  Please choose one of those tasks :   *
    *                                       *
    *  - $ grunt doc                        *
    *  - $ grunt es5                        *
    *                                       *
    =========================================
    `));
};